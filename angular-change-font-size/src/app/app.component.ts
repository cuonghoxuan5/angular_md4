import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title:string  = '';
  age:number    = 21;
  count:number  = 0;
  image:any     = {
    'src': 'https://img.idesign.vn/2019/10/bvb3wzn9ordr8mwvnhrsyd-970-80.jpg',
    'width': 200,
    'height':100,
    'right': 0
  }
  change(event:any){
    this.title = event.target.value  
  }
  add()
  {
    this.count++;
  }
  minus()
  {
    this.count--;
  }

}
