import { Component, OnInit } from '@angular/core';
import {  FormBuilder, Validators,FormGroup } from '@angular/forms';
import { Users } from '../users';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  userform!: FormGroup;
  users: Users[]= [
    {
      name:'Hồ Xuân Cường',
      birthday:'03/08/2000',
      address:'gio linh'
    },
    {
      name:'Nguyễn Sỹ Hạnh',
      birthday:'03/08/1995',
      address:'Đông hà'
    },
  ];
  constructor(
    private _user:FormBuilder
  ) { }

  ngOnInit(): void {
    let newUser:any = {}
    this.userform = this._user.group({
      'name': 
      [
        newUser.name, 
        [ Validators.required]
      ],
      'birthday': 
      [
        newUser.birthday, [Validators.required]
      ],
      'address': 
      [
        newUser.address, [Validators.required]
      ]
     
    });
  }
  isshowform = false;
  showButton(){
    this.isshowform = !this.isshowform;
  }
  handSubmit(){
    console.log(this.userform.value);
    let newUser:Users ={
      name:this.userform.value.name,
      birthday:this.userform.value.birthday,
      address:this.userform.value.address,
    }
    this.users.push(newUser);
    this.isshowform= false;
  }
  delete(index:any){
    this.users.splice(index, 1);
  }
}
