import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { HeaderComponent } from './header/header.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { Routes,RouterModule } from '@angular/router';
import { ProductsComponent } from './products/products.component';
import { ProductsPipe } from './products.pipe';

const routes: Routes = [
  {path : 'users',component: UsersComponent },
  {path : 'products',component: ProductsComponent },
]

@NgModule({
  declarations: [	
    AppComponent,
    UsersComponent,
    HeaderComponent,
// ,    // ProductsComponent
      ProductsComponent,
ProductsPipe
   ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
