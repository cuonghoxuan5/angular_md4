import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Products } from '../products';


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  productform!: FormGroup;
  products: Products[]= [
    {
      name:'Điện thoại IPhone 12 ',
      quantity:'03',
      price:'10000'
    },
    {
      name:'Điện thoại Oppo 13',
      quantity:'08',
      price:'12200'
    },
  ];
  constructor(
    private _product:FormBuilder
  ) { }

  ngOnInit(): void {
    let newProduct:any = {}
    this.productform = this._product.group({
      'name': 
      [
        newProduct.name, [ Validators.required]
      ],
      'quantity': 
      [
        newProduct.quantity, [Validators.required]
      ],
      'price': 
      [
        newProduct.price, [Validators.required]
      ]
     
    });
  }
  isshowform = false;
  showButton(){
    this.isshowform = !this.isshowform;
  }
  handSubmit(){
    console.log(this.productform.value);

    let newProduct:Products ={
      name:this.productform.value.name,
      quantity:this.productform.value.quantity,
      price:this.productform.value.price,
    }
    this.products.push(newProduct);
    this.isshowform= false;
  }
  delete(index:any){
    this.products.splice(index, 1);
  }
}
