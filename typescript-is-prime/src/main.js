function Sum(number) {
    var Sum = true;
    if (number < 2) {
        Sum = false;
    }
    else if (number > 2) {
        for (var i = 2; i <= Math.sqrt(i); i++) {
            if (number % i == 0) {
                Sum = false;
                break;
            }
        }
    }
    return Sum;
}
var array = [1, 5, 9, 2, 6, 15, 19, 35, 51, 53];
var sum = 0;
for (var _i = 0, array_1 = array; _i < array_1.length; _i++) {
    var number = array_1[_i];
    if (Sum(number)) {
        sum += number;
    }
}
console.log("Tổng các số nguyên tố trong mảng trên là: " + sum);
