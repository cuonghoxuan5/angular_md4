import {Component, OnInit} from '@angular/core';
import {Todo} from '../todo';
import {FormControl, FormGroup, Validators} from '@angular/forms';

let _id = 1;

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {
  // khởi tạo một mảng các đối tượng todo 
  todos: Todo[] = [];

  // khởi tạo một đối tượng FormControl có tên là content
  content = new FormControl();

  constructor() {
  }

  ngOnInit() {
  }

  toggleTodo(i: number) {
    this.todos[i].complete = !this.todos[i].complete;
    // nếu click thì sẽ xuất hiện 1 mảng tương tự đã gõ 
  }

  change() {
    const value = this.content.value;
    // gán cho value vào 1 giá trị 
    if (value) {
      // nếu value có giá trị thì sẽ xuất hiện 1 mảng có id và value xuất hiện ở màn hình
      const todo: Todo = {
        id: _id++,
        content: value,
        complete: false
      };
      this.todos.push(todo);
      // thêm và hiển thị 1 mảng 
      this.content.reset();
      
    }
  }
}