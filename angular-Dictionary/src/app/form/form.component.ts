import { Component, OnInit } from '@angular/core';
import { Form } from '../form';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  forms:Form[]=
  [
    {
      name :'Oppo x09',
      quantity : 30,
      price : "12.000.000 VND",
      code : '#CfJGHWEHJ1'
    },
    {
      name :'IPhone  X',
      quantity : 100,
      price : "24.000.000 VND",
      code : '#Cf4FHJddn'
    },
    {
      name :'Redmi',
      quantity : 60,
      price : "13.909.000 VND",
      code : '#CfJGfGHJB12'
    }
  ]
  newform:Form={
    name:'',
    quantity:'',
    price:'',
    code:'',
  }
  constructor() { }

  ngOnInit(): void {
  }
  isshowForm:any = false;

  delete(index:any){
    this.forms.splice(index,1)
  }

  clickshowForm(){
    this.isshowForm = !this.isshowForm
  }
  // thêm dữ liệu vào bảng 
  handSubmit(fileForm:any)
  {
   let newform:Form = 
   {
     name:fileForm.name,
     quantity:fileForm.quantity,
     price:fileForm.price,
     code:fileForm.code
   }
   this.forms.push(newform);
   this.isshowForm = false;
  }
}
