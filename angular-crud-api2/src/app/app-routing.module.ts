import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductAddComponent } from './product-add/product-add.component';
import { ProductDeleteComponent } from './product-delete/product-delete.component';
import { ProductEditComponent } from './product-edit/product-edit.component';
import { ProductsComponent } from './products/products.component';

const routes: Routes = [
  {path:'' , component: ProductsComponent},
  {path:'product-add' , component: ProductAddComponent},
  {path:'product-edit/:id' , component: ProductEditComponent},
  {path:'product-delete/:id' , component: ProductDeleteComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
