import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Product } from './product';

const API_URL = `${environment.apiUrl}`;
@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  constructor(private http:HttpClient) { }
  getAll():Observable<Product[]>
  {
    return this.http.get<Product[]>(API_URL + '/products');
  }
  find(id:any):Observable<Product>
  {
    return this.http.get<Product>(`${API_URL}/products/${id}`)
  }
  store(product:Product):Observable<Product>
  {
    return this.http.post<Product>(API_URL + '/products',product)
  }
  update(id:any,product:Product):Observable<Product>
  {
    return this.http.put<Product>(`${API_URL}'/products/${id}'` ,product)
  }
  destroy(id:any):Observable<Product>
  {
    return this.http.delete<Product>(`${API_URL}/products/${id}`)
  }
  search(q:any):Observable<Product[]>
  {
    return this.http.get<Product[]>(API_URL + '/products/?search='+q)
  }
}
