import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from '../product';
import { ProductsService } from '../products.service';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html'
})
export class ProductsComponent implements OnInit {
  products:Product[] =[]
  constructor(
    private _Router:Router,
    private _ProductsService:ProductsService,
    private __ActivatedRoute : ActivatedRoute
  ) { }

  ngOnInit(): void {
    this._ProductsService.getAll().subscribe(products =>{this.products = products ;})
  }
  search(value:any){
    if(value){
      this._ProductsService.search(value).subscribe(products =>{this.products = products ;})
    }else{
      this._ProductsService.getAll().subscribe(products =>{this.products = products ;})
    }
  }

}
