import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from '../product';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-product-add',
  templateUrl: './product-add.component.html'
})
export class ProductAddComponent implements OnInit {

  productForm!:FormGroup

  constructor(
    private _ProductService : ProductService,
    private _Router : Router,
    private __ActivatedRoute : ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.productForm = new FormGroup({
      name:new FormControl('', [Validators.required]),
      author: new FormControl('',[ Validators.required]),
      description: new FormControl('',[ Validators.required]),
    });
  }

  onSubmit()
  {
    let formData = this.productForm.value;
    let product:Product={
      name:formData.name,
      author:formData.author,
      description:formData.description
    }
    this._ProductService.store(product).subscribe(()=>
    {
      this.productForm.reset();

      //redirect to products
      this._Router.navigate(['/products']);
    }, e => { console.log(e); });
  }


}
