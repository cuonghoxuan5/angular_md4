import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Product } from './product';

const API_URL = `${environment.apiUrl}`;

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  constructor(private http:HttpClient) { }
  getAll():Observable<Product[]>
  {
    return this.http.get<Product[]>(API_URL + '/products');
  }
  // lấy 1 phần tử
  find(id:any):Observable<Product>
  {
    return this.http.get<Product>(`${API_URL}/products/${id}`)
  }
  // tạo 
  store(product:Product):Observable<Product>
  {
    return this.http.post<Product>(API_URL + '/products',product);
  }
  // sửa
  update(id:any , product:Product):Observable<Product>
  {
    return this.http.put<Product>(`${API_URL}/products/${id}`, product);
  }
  // xóa 
  destroy(id:any):Observable<Product>
  {
    return this.http.delete<Product>(`${API_URL}/products/${id}`)
  }
  // tìm kiếm 
  search(q:any):Observable<Product[]>
  {
    return this.http.get<Product[]>(API_URL + '/products/?search='+q);
  }
}
