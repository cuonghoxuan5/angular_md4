import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Product } from '../product';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html'
})
export class ProductEditComponent implements OnInit {

  id: any = 0;
  
  productForm!: FormGroup;

  constructor(
    private _ActivatedRoute: ActivatedRoute,
    private _ProductService: ProductService,
    private _Router:Router
  ) { }

  ngOnInit(): void 
  {
    //get id from url
    this._ActivatedRoute.paramMap.subscribe((paramMap: ParamMap) => 
    {
      const id = paramMap.get('id');
      
      //thay doi gia tri thuoc tinh de su dung cho edit
      this.id = id;
      
     this._ProductService.find(id).subscribe(product=>{
       this.productForm = new FormGroup({
         name:new FormControl(product.name,[Validators.required,Validators.minLength(5)]),
         author:new FormControl(product.author,[Validators.required]),
         description:new FormControl(product.description,[Validators.required])
       });
     });
    });
  }
  onSubmit() {
    //handle submit form
    let formData = this.productForm.value;
    let product: Product = {
      name: formData.name,
      author: formData.author,
      description: formData.description
    }
    this._ProductService.update(this.id, product).subscribe(() => {
      //redirect to products
      this._Router.navigate(['/products']);
    }, e => {
      console.log(e);
    });
  }
}
