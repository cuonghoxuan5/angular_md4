import { Injectable } from '@angular/core';
import { Song } from '../model/song';

@Injectable({
  providedIn: 'root'
})
export class SongService {
  playlist: Song[] = [
    {
      id: 'C8868vMfHHc',
      name: 'Suốt đời không xứng'
    },
    {
      id: 'Bzqv9z47KRs',
      name: 'Đừng Lo Anh Chờ Mà - Remix'
    },
    {
      id: '-72_-ks_CMQ',
      name: 'Đế Vương'
    },
    {
      id: 'v4JXyqhRRa8',
      name: 'Đá bóng'
    },
    {
      id: 'c-nIXcVoou4',
      name: 'game-lmht'
    }
  ];
  constructor() { }
  findSongById(id: string) {
    return this.playlist.find(item => item.id === id);
  }

}
