import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { ProductsComponent } from "./components/products.component";
import { ProductsAddComponent } from "./components/products-add.component";
import { ProductsEditComponent } from "./components/products-edit.component";
import { ProductsDeleteComponent } from "./components/products-delete.component";

import { AdminProductRoutingModule } from './admin-product-routing.module';

@NgModule({
  declarations: [
    ProductsComponent,
    ProductsAddComponent,
    ProductsEditComponent,
    ProductsDeleteComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AdminProductRoutingModule
  ]
})
export class AdminProductModule { }