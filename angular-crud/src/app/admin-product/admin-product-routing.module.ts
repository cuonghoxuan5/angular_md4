import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ProductsComponent } from "./components/products.component";
import { ProductsAddComponent } from "./components/products-add.component";
import { ProductsEditComponent } from "./components/products-edit.component";
import { ProductsDeleteComponent } from "./components/products-delete.component";

const routes: Routes = [
  {path: '',component: ProductsComponent},
  {path: 'list',component: ProductsComponent}, 
  {path: 'product-add',component: ProductsAddComponent},
  {path: 'product-edit/:id',component: ProductsEditComponent},
  {path: 'product-delete/:id',component: ProductsDeleteComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminProductRoutingModule { }