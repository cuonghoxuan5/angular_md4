import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { ProductsService } from '../../shared/service/products.service';
import { Products } from '../../shared/defines/products';


@Component({
  selector: 'app-products-delete',
  templateUrl: '.././teamplate/products-delete.component.html'
})

export class ProductsDeleteComponent implements OnInit {
  id:any = 0;
 
  product!: Products;

  constructor(
    private _ActivatedRoute: ActivatedRoute,
    // thực hiện route trên url 
    private _ProductService: ProductsService,
    // tiêm các phương thức ở service 
    private _Router:Router
    // truyển các thanh router
  ) { }

  ngOnInit(): void {
    this._ActivatedRoute.paramMap.subscribe((paramMap: ParamMap) => {
      const id = paramMap.get('id');
      this.id = id;
      this.product = this._ProductService.find(id);
    });
  }

    // // thực hiện xóa rồi chuyển hướng về trang product
    // handleYes(){
    //   this._ProductService.destroy(this.id);
    //   //redirect to products
    //   this._Router.navigate(['/products']);
    // }

    // // cancel không xóa thì load về lại trang sản phẩm
    // handleNo(){
    //   //redirect to products
    //   this._Router.navigate(['/products']);
    // }

}
