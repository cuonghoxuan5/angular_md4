import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { ProductsService } from '../../shared/service/products.service';
import { Products } from '../../shared/defines/products';

@Component({
  selector: 'app-products-edit',
  templateUrl: '.././teamplate/products-edit.component.html'
})
export class ProductsEditComponent implements OnInit {
  // khởi tạo id = 0
  id: any = 0;
  
  productForm!: FormGroup;
  constructor(
    private _ActivatedRoute: ActivatedRoute,
    private _ProductService: ProductsService,
    private _Router: Router
  ) { }

  ngOnInit(): void {
    //get id from url
    // subscribe hàm có sẵn giúp theo dõi thay đổi dữ liệu và trả về kết qủa của 1 vc nào đó
    this._ActivatedRoute.paramMap.subscribe((paramMap: ParamMap) => {
      // gán id 
      const id = paramMap.get('id');

      // thay đổi giá trị của id 
      this.id = id;
      
      let product = this._ProductService.find(id);
     

      //fill input to form
      this.productForm = new FormGroup({
        name: new FormControl(product.name, [
          Validators.required,
          Validators.minLength(5)
        ]),
        price: new FormControl(product.price, [
          Validators.required
        ]),
      });

      //build with reactiform form
    });
  }
  onSubmit() {
    let formData = this.productForm.value;
    console.log(formData);

    let product: Products = {
      name: formData.name,
      price: formData.price
    }
    // call service update
    this._ProductService.update(this.id,product);
    console.log(this._ProductService);
    this._Router.navigate(['/products']);
  }
}
