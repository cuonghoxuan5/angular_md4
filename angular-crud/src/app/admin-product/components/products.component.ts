import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Products } from '../../shared/defines/products';
import { ProductsService } from '../../shared/service/products.service';
@Component({
  selector: 'app-products',
  templateUrl: '.././teamplate/products.component.html',
})
export class ProductsComponent implements OnInit {
  products: Products[] = [];
  idDelete:any = null;

  constructor( 
    private _ProductService:ProductsService,
    private _Router:Router
    ) { }

  ngOnInit(): void {
    this.products = this._ProductService.getAll();
  }
  search(value:any){
    if(value){
      this.products = this._ProductService.search(value);
    }else{
      this.products = this._ProductService.getAll();
    }
  }
  handleYes(idDelete:any){
    this._ProductService.destroy(this.idDelete);
    //redirect to products
    
    this._Router.navigate(['/products']);
    // this.idDelete = null;
  }
}
