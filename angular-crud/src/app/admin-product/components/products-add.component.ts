import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProductsService } from '../../shared/service/products.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Products } from '../../shared/defines/products';

@Component({
  selector: 'app-products-add',
  templateUrl: '.././teamplate/products-add.component.html',
})
export class ProductsAddComponent implements OnInit {
  productForm!: FormGroup;
  constructor(
    private _ActivatedRoute: ActivatedRoute,
    private _ProductService: ProductsService,
    private _Router:Router
  ) { }

  ngOnInit(): void {
    this.productForm = new FormGroup({
      name:new FormControl('',[Validators.required,Validators.minLength(5)]),
      price:new FormControl('',[Validators.required])
    })
  }
  onSubmit(){
    let formData = this.productForm.value;
    let product: Products = {
      name: formData.name,
      price: formData.price
    }
    // call service update
    this._ProductService.store(product);

    //redirect to products
    this._Router.navigate(['/products']);
  }
}
