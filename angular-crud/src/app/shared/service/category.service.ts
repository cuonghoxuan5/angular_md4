import { Injectable } from '@angular/core';
import { Category } from '../defines/category';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  categories:Category[]=[
    {
      name:'Điện Thoại',
      status:'Còn hàng',
      code:'#VBSAF5sdd'
    },
    {
      name:'LapTop',
      status:'Còn hàng',
      code:'#VBSfaf5ad'
    },
    {
      name:'Máy nghe nhạc',
      status:'Còn hàng',
      code:'#VBSAFAGg23'
    },
  ]
  constructor() { }
  getAll():Category[]{
    return this.categories;
  }
  find(id:any):Category{
    return this.categories[id];
  }
  store(category:Category):void{
    this.categories.push(category)
  }
  update(id:any,category:Category):void{
      this.categories[id] = category
  }
  destroy(id:any){
    this.categories.splice(id,1)
  }
  search(q:any):Category[] {
    let resulf:Category[] = [];
    for(let category of this.categories){
      if(category.name == q){
        resulf.push(category);
      }
    }
    return resulf;
  }
}
