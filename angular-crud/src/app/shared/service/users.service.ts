import { Injectable } from '@angular/core';
import { User } from '../defines/user';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  users: User[] = [
    {
      name:'Nhi',
      birthday:'03/08/1999',
      address: 'gio linh'
    },
    {
      name:'Hạnh',
      birthday:'03/08/1995',
      address: 'đông hà'
    },
    {
      name:'Nam',
      birthday:'03/08/1990',
      address: 'đông hà'
    },
    
  ];
  constructor() { }
  
  getAll():User[] {
    // lấy tất cả các dữ liệu ở products trên 
    return this.users
  }
  store(users:User):void{
    this.users.push(users);
  }
  update(id:any ,user:User):void{
    this.users[id] = user;
  }
   // lấy 1 phần tử theo id
   find(id:any):User{
    return this.users[id];
  }
  destroy(id:any){
    this.users.splice(id,1)
  }

  search(q:any):User[]{
    let results:User[] = [];
    for(let user of this.users){
      if(user.name == q){
        results.push(user);
      }
    }
    return results;
  }
}
