import { Injectable } from '@angular/core';
import { Products } from '../defines/products';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  products: Products[] = [
    {
      name:'Iphone',
      price:'20000'
    },
    {
      name:'SamSung',
      price:'20000'
    },
    {
      name:'Oppo',
      price:'20000'
    },
  ];
  constructor() { }
  getAll():Products[] {
    // lấy tất cả các dữ liệu ở products trên 
    return this.products
  }
  // lấy 1 phần tử theo id
  find(id:any):Products{
    return this.products[id];
  }
  store(products:Products):void{
    this.products.push(products);
  }
  update(id:any ,product:Products):void{
    this.products[id] = product;
  }
  destroy(id:any){
    this.products.splice(id,1)
  }

  search(q:any):Products[]{
    let results:Products[] = [];
    for(let product of this.products){
      if(product.name == q){
        results.push(product);
      }
    }
    return results;
  }
}
