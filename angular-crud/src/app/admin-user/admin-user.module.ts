import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersComponent } from './components/users.component';
import { UsersDeleteComponent } from './components/users-delete.component';
import { UsersEditComponent } from './components/users-edit.component';
import { UsersAddComponent } from './components/users-add.component';

import { AdminUserRoutingModule } from './admin-user-routing.module';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    UsersComponent,
    UsersAddComponent,
    UsersEditComponent,
    UsersDeleteComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AdminUserRoutingModule,
    
  ]
})
export class AdminUserModule { }
