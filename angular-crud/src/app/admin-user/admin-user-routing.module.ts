import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UsersAddComponent } from './components/users-add.component';
import { UsersDeleteComponent } from './components/users-delete.component';
import { UsersEditComponent } from './components/users-edit.component';
import { UsersComponent } from './components/users.component';

const routes : Routes = [ 
    { path: '', component: UsersComponent },
    { path: 'users-add', component: UsersAddComponent },
    { path: 'users-edit/:id', component: UsersEditComponent },
    { path: 'users-delete/:id', component: UsersDeleteComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
export class AdminUserRoutingModule { }