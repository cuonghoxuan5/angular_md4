import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { User } from '../../shared/defines/user';
import { UsersService } from '../../shared/service/users.service';

@Component({
  selector: 'app-users-edit',
  templateUrl: '.././template/users-edit.component.html'
})
export class UsersEditComponent implements OnInit {
  id:any = 0;

  userForm!: FormGroup;
  constructor(
    private _UserServive : UsersService,
    private _Router:Router,
    private _ActivatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this._ActivatedRoute.paramMap.subscribe((paramMap: ParamMap) => {
      const id = paramMap.get('id');
      this.id = id;
      // console.log(id)
      let user = this._UserServive.find(id)

      this.userForm = new FormGroup({
        name:new FormControl(user.name,[Validators.required]),
        birthday: new FormControl(user.birthday,[Validators.required]),
        address:new FormControl(user.address,[Validators.required])
      })
    })
  } 
  onSubmit() {
    let userData = this.userForm.value;
    
    let user:User={
      name:userData.name,
      birthday:userData.birthday,
      address:userData.address
    }
    console.log(user);
    this._UserServive.update(this.id,user)
    this._Router.navigate(['/users'])
  }

}
