import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { User } from '../../shared/defines/user';
import { UsersService } from '../../shared/service/users.service';

@Component({
  selector: 'app-users-delete',
  templateUrl: '.././template/users-delete.component.html'
})
export class UsersDeleteComponent implements OnInit {
  id:any = 0 ;
  user!:User;
  constructor(
    private _UserService :UsersService,
    private _ActivatedRoute: ActivatedRoute,
    private _Router:Router
  ) { }

  ngOnInit(): void {
    this._ActivatedRoute.paramMap.subscribe((paramMap: ParamMap)=>{
      const id = paramMap.get('id');
      this.id = id;
      this.user = this._UserService.find(id);
    });
  }
  handleYes(){
    this._UserService.destroy(this.id);
    this._Router.navigate(['/users'])
  }
  handleNo(){
    this._Router.navigate(['/users'])
  }

}
