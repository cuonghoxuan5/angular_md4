import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { UsersService } from '../../shared/service/users.service';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../../shared/defines/user';

@Component({
  selector: 'app-users-add',
  templateUrl: '.././template/users-add.component.html'
})
export class UsersAddComponent implements OnInit {
  userForm!:FormGroup;

  constructor(
    private _UserService :UsersService,
    private _ActivatedRoute :ActivatedRoute,
    private _Router:Router
    ) { }

  ngOnInit(): void {
    this.userForm = new FormGroup({
      name:new FormControl('',[Validators.required]),
      birthday: new FormControl('',[Validators.required]),
      address: new FormControl('',[Validators.required])
    })
  }
  onSubmit() {
    let userData = this.userForm.value;
    let user:User ={
      name: userData.name,
      birthday: userData.birthday,
      address: userData.address
    } 
    this._UserService.store(user);
    this._Router.navigate(['/users']);
  }

}
