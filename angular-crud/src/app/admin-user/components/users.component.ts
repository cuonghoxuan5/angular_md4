import { Component, OnInit } from '@angular/core';
import { User } from '../../shared/defines/user';
import { UsersService } from '../../shared/service/users.service';

@Component({
  selector: 'app-users',
  templateUrl: '.././template/users.component.html',
})
export class UsersComponent implements OnInit {
  users: User[] = [];
  constructor(private _UserService:UsersService) { }

  ngOnInit(): void {
    this.users = this._UserService.getAll();
  }
  search(value:any){
    if(value){
      this.users = this._UserService.search(value);
    }else{
      this.users = this._UserService.getAll();
    }
    
  }
}
