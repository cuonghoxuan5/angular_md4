import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CategoriesComponent } from './components/categories.component';
import { CategoryAddComponent } from './components/category-add.component';
import { CategoryDeleteComponent } from './components/category-delete.component';
import { CategoryEditComponent } from './components/category-edit.component';

const routes : Routes = [
    {path:'', component:CategoriesComponent},
    {path:'list', component:CategoriesComponent},
    {path:'category-add', component:CategoryAddComponent},
    {path:'category-edit/:id', component:CategoryEditComponent},
    {path:'category-delete/:id', component:CategoryDeleteComponent},
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class AdminCategoryRoutingModule { }