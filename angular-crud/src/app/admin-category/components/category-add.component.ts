import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Category } from 'src/app/shared/defines/category';
import { CategoryService } from 'src/app/shared/service/category.service';

@Component({
  selector: 'app-category-add',
  templateUrl: '.././template/category-add.component.html'
})
export class CategoryAddComponent implements OnInit {
  categoryForm!:FormGroup;

  constructor(
    private _ActivatedRoute:ActivatedRoute,
    private _Router:Router,
    private _CategoryService:CategoryService
  ) { }

  ngOnInit(): void {
    this.categoryForm = new FormGroup({
      name:new FormControl('',[Validators.required]),
      status:new FormControl('',[Validators.required]),
      code:new FormControl('',[Validators.required]),
    })
  }
  onSubmit(){
    // handle submit form 
    let formData = this.categoryForm.value;
    let category:Category = {
      name:formData.name,
      status:formData.status,
      code:formData.code
    }
    this._CategoryService.store(category)
    this._Router.navigate(['/categories'])
  }

}
