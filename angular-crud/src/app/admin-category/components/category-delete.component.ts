import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Category } from 'src/app/shared/defines/category';
import { CategoryService } from 'src/app/shared/service/category.service';

@Component({
  selector: 'app-category-delete',
  templateUrl: '.././template/category-delete.component.html',
})
export class CategoryDeleteComponent implements OnInit {
  id:any= 0;
  category!:Category;

  constructor( 
    private _ActivatedRoute:ActivatedRoute,
    private _Router:Router,
    private _CategoryService:CategoryService
    ) { }

  ngOnInit(): void {
    this._ActivatedRoute.paramMap.subscribe((paramMap:ParamMap)=>{
      const id = paramMap.get('id')
      this.id = id;
      this.category = this._CategoryService.find(id);
    });
  }
  handleYes(){
    this._CategoryService.destroy(this.id)
    this._Router.navigate(['/categories'])
  }
  handleNo(){
    this._Router.navigate(['/categories'])
  }
}
