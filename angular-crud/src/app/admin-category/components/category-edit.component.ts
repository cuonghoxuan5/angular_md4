import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Category } from 'src/app/shared/defines/category';
import { CategoryService } from 'src/app/shared/service/category.service';

@Component({
  selector: 'app-category-edit',
  templateUrl: '.././template/category-edit.component.html',
})
export class CategoryEditComponent implements OnInit {
  id:any = 0;
  categoryForm!: FormGroup;

  constructor(
    private _ActivatedRoute: ActivatedRoute,
    private _CategoryService: CategoryService,
    private _Router:Router
  ) { }

  ngOnInit(): void 
  {
    this._ActivatedRoute.paramMap.subscribe((paramMap: ParamMap)=>{
      const id = paramMap.get('id');
      this.id = id;

      let category = this._CategoryService.find(id);

      this.categoryForm = new FormGroup({
        name:new FormControl(category.name,[Validators.required]),
        status:new FormControl(category.status,[Validators.required]),
        code:new FormControl(category.code,[Validators.required])
      });
    });
  }
  onSubmit()
  { 
    let formData = this.categoryForm.value;
      let category: Category = 
      {
        name: formData.name,
        status: formData.status,
        code: formData.code,
      }
      // call service update
      this._CategoryService.update(this.id,category);

      //redirect to products
      this._Router.navigate(['/categories']);
    }
  }
