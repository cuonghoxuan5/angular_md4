import { Component, OnInit } from '@angular/core';
import { Category } from 'src/app/shared/defines/category';
import { CategoryService } from 'src/app/shared/service/category.service';

@Component({
  selector: 'app-categories',
  templateUrl: '.././template/categories.component.html',
})
export class CategoriesComponent implements OnInit {
  categories:Category[]=[];

  constructor(
    private _CategoryService:CategoryService
  ) { }

  ngOnInit(): void {
    this.categories = this._CategoryService.getAll()
  }
  search(value:any){
    if(value){
      this.categories = this._CategoryService.search(value);
    }else{
      this.categories = this._CategoryService.getAll();
    }
  }
}
