import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoryAddComponent } from './components/category-add.component';
import { CategoriesComponent } from './components/categories.component';
import { CategoryDeleteComponent } from './components/category-delete.component';
import { CategoryEditComponent } from './components/category-edit.component';
import { ReactiveFormsModule } from '@angular/forms';
import { AdminCategoryRoutingModule } from './admin-category-routing.module';



@NgModule({
  declarations: [
    CategoryAddComponent,
    CategoriesComponent,
    CategoryDeleteComponent,
    CategoryEditComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AdminCategoryRoutingModule
  ]
})
export class AdminCategoryModule { }
