import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./admin-product/admin-product.module').then(module => module.AdminProductModule)
  },
  {
    path: 'products',
    loadChildren: () => import('./admin-product/admin-product.module').then(module => module.AdminProductModule)
  },
  {
    path: 'admin/products',
    loadChildren: () => import('./admin-product/admin-product.module').then(module => module.AdminProductModule)
  },
  {
    path: 'users',
    loadChildren: () => import('./admin-user/admin-user.module').then(module => module.AdminUserModule)
  },
  {
    path: 'categories',
    loadChildren: () => import('./admin-category/admin-category.module').then(module => module.AdminCategoryModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
