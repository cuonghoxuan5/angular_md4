import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Product } from '../product';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-product-delete',
  templateUrl: './product-delete.component.html'
})
export class ProductDeleteComponent implements OnInit {
  id: any = 0;
  product!: Product;

  constructor(
    private _ActivatedRoute: ActivatedRoute,
    private _ProductService: ProductService,
    private _Router:Router
  ) { }

  ngOnInit(): void {
    this._ActivatedRoute.paramMap.subscribe((paramMap: ParamMap) => {
      const id = paramMap.get('id');

      this.id = id;

      this._ProductService.find(id).subscribe(product => {
        this.product = product;
      });
    });
  }

  handleYes(){
    this._ProductService.destroy(this.id).subscribe(() => {
    this._Router.navigate(['/product']);
    }, (e: any) => {
      console.log(e);
    });
    //redirect to products
    //this._Router.navigate(['/products']);
  }
  handleNo(){
    //redirect to products
    this._Router.navigate(['/product']);
  }


}
