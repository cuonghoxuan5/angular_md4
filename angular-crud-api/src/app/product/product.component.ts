import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from '../product';
import { ProductService } from '../product.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html'
})
export class ProductComponent implements OnInit {
  // idDelete:any = null;

  products:Product[] = []

  constructor(
    private _ProductService:ProductService,
    private _Router:Router
  ) { }

  ngOnInit(): void {
    this._ProductService.getAll().subscribe(products => {this.products = products;})
  }
  search(value:any)
  {
    if(value)
    {
      this._ProductService.search(value).subscribe(products => {this.products = products;})
    }else{
      this._ProductService.getAll().subscribe(products=>{this.products = products;});
    }
  }

}
