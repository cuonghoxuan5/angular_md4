import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent implements OnInit {
  number1:any = '';
  number2:any = '';
  method:any = '';
  result:any = 0;
  constructor() { }

  ngOnInit(): void {
  }
  handTinh(){
  
    switch(this.method){
      case 'cong':
        this.result = this.number1 + this.number2
        break;
      case 'tru':
        this.result = this.number1 - this.number2
        break;
      case 'nhan':
        this.result = this.number1 * this.number2
        break;
      case 'chia':
        this.result = this.number1 / this.number2
        break;
    }
  }

}
