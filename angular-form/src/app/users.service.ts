import { Injectable } from '@angular/core';
import { Users } from './users';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor() { }
  // danh  sách 
  all():Users[]{
    let  users:Users[] = [
        {
          name: 'Hồ Xuân Cường',
          gender:true,
          birthday: '03/08/2000',
          address:'Phong Bình ,Quảng Trị'
        },
        {
          name: 'Nguyễn Thảo Nhi',
          gender:false,
          birthday: '03/08/1999',
          address:'Phong Bình ,Quảng Trị'
        }, 
        {
          name: 'Nhi ÓC Chó',
          gender:false,
          birthday: '03/08/2000',
          address:'Phong Bình ,Quảng Trị'
        }
      ]
      return users
  }
 // tạo
  create(): void{

  }
  // xem 
  read(): void{

  }
  // Sửa
  update(): void{

  }
  // Xóa
  delete(): void{

  }
  
}
