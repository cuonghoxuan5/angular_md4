import { Component, OnInit } from '@angular/core';
import { Users } from '../users'; 
import { UsersService } from '../users.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  /* 
  b1 : tạo service ng g s users
  b2 : import vào component 
  b3 : tiêm contructor
  b4 : gọi phương thức tất cả data
  */

  users: Users[] = [];
    newUser:Users={
      name:'',
      gender:'',
      birthday:'',
      address:'',
    }
  // chuyển khai báo này sang service
  // 
//     users:Users[] = [
//         {
//           name: 'Hồ Xuân Cường',
//           gender:true,
//           birthday: '03/08/2000',
//           address:'Phong Bình ,Quảng Trị'
//         },
//         {
//           name: 'Nguyễn Thảo Nhi',
//           gender:false,
//           birthday: '03/08/1999',
//           address:'Phong Bình ,Quảng Trị'
//         }, 
//         {
//           name: 'Nhi ÓC Chó',
//           gender:false,
//           birthday: '03/08/2000',
//           address:'Phong Bình ,Quảng Trị'
//         }
//       ]
// }
  constructor(
    private usersService:UsersService,
  ) { }

  ngOnInit(): void {
    this.users = this.usersService.all();
  }
  ishowform: boolean = false;
  toggleForm(){
    this.ishowform = !this.ishowform;
  }
  delete(index:number){
    this.users.splice(index,1);
  }
  handSubmit(fileForm:any )
  {
    
    console.log(fileForm);
    
    var newUser:Users = 
    {
      name:fileForm.name,
      gender:fileForm.gender,
      birthday:fileForm.birthday,
      address:fileForm.address
    }
    this.users.push(newUser);
    this.ishowform = false;
  }
  
}