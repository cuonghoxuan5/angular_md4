import { Component, OnInit } from '@angular/core';
import { Products } from '../products';
import { FormGroup, Validators, FormBuilder} from '@angular/forms';
// ReactiveFormsModule khai báo nó ở app.module.ts 

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  // step 1 
  fileForm!: FormGroup;

  products: Products[] = 
  [
    {
      name:'IP X 126gb',
      price: '13000' 
    },
    {
      name:'IP 12 126gb',
      price: '23000' 
    },
    {
      name:'IP 13 126gb',
      price: '33000' 
    }
  ];
  //  step 2
  constructor( private _formBuilder:FormBuilder) {}
            
  
  ngOnInit(): void {
    // step 3
     let newProduct:any = {}

    this.fileForm = this._formBuilder.group({
      'name': 
      [
        newProduct.name, 
        [
          Validators.required,
          // không đc để trống 
          Validators.minLength(6),
          // minlength giới hạn kí tự 
        ]
      ],
      'price': 
      [
        newProduct.price, []
      ]
     
    });
  }
  ishowform: boolean = false;
  toggleForm()
  {
    this.ishowform = !this.ishowform;
  }
  deleteItem(index:number)
  {
    this.products.splice(index,1);
  }
  handSubmit()
  {
    //lấy giá trị form ra
    var newProduct:Products = 
    {
      name:this.fileForm.value.name,
      // lấy value của name 
      price:this.fileForm.value.price
      // lấy value của price 
    }
    this.products.push(newProduct);
    this.ishowform = false; 
  } 
}
