import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { ProductsComponent } from './products/products.component';
import { UsersComponent } from './users/users.component';
import { ProductComponent } from './product/product.component';
import { NumberFormatPipe } from './number-format.pipe';
import {RouterModule, Routes } from '@angular/router';
import { UsersCreateComponent } from './users-create/users-create.component';
import { HeaderComponent } from './header/header.component';

const routes:Routes = [
                          {path : 'users',component: UsersComponent },
                          {path : 'users_create',component: UsersCreateComponent },
                      ]

@NgModule({
  declarations: [
    AppComponent,
    ProductsComponent,
    UsersComponent,
    ProductComponent,
    NumberFormatPipe,
    UsersCreateComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes)
   
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
