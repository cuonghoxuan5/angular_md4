import { Component, Input, OnInit, Output ,EventEmitter } from '@angular/core';


@Component({
  selector: '[app-product]',
  templateUrl: './product.component.html',
})
export class ProductComponent implements OnInit {
 
  @Input("input_product") product:any;
  @Input("input_i") i:any;

  @Output('handleDeleteItem') handleDeleteItem = new EventEmitter<any>();
  // khai báo output handleDeleteItem vs tên mới 
  constructor() { }

  ngOnInit(): void {
  }
  handDelete(index:any){
    this.handleDeleteItem.emit(index);
    // emit sẽ truyền ngược lại cho products 
  }
 
}
