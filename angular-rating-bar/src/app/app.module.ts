import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IRatingUnitPipe } from './irating-unit.pipe';
import { IRatingUnitComponent } from './irating-unit/irating-unit.component';

@NgModule({
  declarations: [
    AppComponent,
    IRatingUnitPipe,
    IRatingUnitComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
