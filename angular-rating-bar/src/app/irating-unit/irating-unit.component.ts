import { Component, Input, OnInit,Output ,EventEmitter, SimpleChanges} from '@angular/core';
import { IRatingUnit } from '../irating-unit';

@Component({
  selector: 'app-irating-unit',
  templateUrl: './irating-unit.component.html',
  styleUrls: ['./irating-unit.component.css']
})
export class IRatingUnitComponent implements OnInit {
  @Input()
  max:any = 20;
  // cho 20 ô
  @Input()
  ratingValue:any = 0;
  //hiển thị khởi đẩu từ số 0 
  @Input()
  showRatingValue:any= true;
  // hiển thị ratingValue 

  @Output()
  rateChange:any= new EventEmitter<number>();

  ratingUnits: Array<IRatingUnit> = [];

  constructor() { }

  ngOnChanges(changes:any) {
    // if ('max' in changes) {
    //   let max = changes.max.currentValue;
    //   max = typeof max === 'undefined' ? 5 : max;
    //   this.max = max ;
    //   // this.calculate(max, this.ratingValue);
    // }
  }

  calculate(max:any, ratingValue:any) {
    this.ratingUnits = Array.from({length: max},
      (_, index) => ({
        value: index + 1,
        active: index < ratingValue
      }));
  }

  // ngOnit hàm khởi tạo 
  ngOnInit() {
    this.calculate(this.max, this.ratingValue);
    // lấy đối tượng caculate là số ô và điểm bắt đầu  
  }

  select(index:any ) {
    this.ratingValue = index + 1;
    // khi trỏ chuột đến đâu ratingValue tăng i lên tới đấy 

    this.ratingUnits.forEach((item, idx) => item.active = idx < this.ratingValue);
    
    this.rateChange.emit(this.ratingValue);
    
  } 
  enter(index:any) {
    this.ratingUnits.forEach((item, idx) => item.active = idx <= index);
  }
  reset() {
    this.ratingUnits.forEach((item, idx) => item.active = idx < this.ratingValue);
  }

}