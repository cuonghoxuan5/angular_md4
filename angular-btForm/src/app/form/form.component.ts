import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup,  Validators } from '@angular/forms';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.css']
})
export class FormComponent implements OnInit {
  form!: FormGroup;

  constructor(private fb: FormBuilder) { 
  }
 
  ngOnInit(): void  
  {
    this.form = this.fb.group({
      'email':['', [Validators.required,Validators.email]],
      'password':['',[Validators.minLength(6) ,Validators.required]],
      'confirm_password':['',[Validators.required]],
      'country':['', [Validators.required]],
      'gender':['', [Validators.required]],
      'age':['',[Validators.required]],
      'phone':['',[Validators.required]]
    })
    
  }
  onSubmit() {
    console.log(this.form.value);
  }
}
// ngModel hiển thị value của các ô input