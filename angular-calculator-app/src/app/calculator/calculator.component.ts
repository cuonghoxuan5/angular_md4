import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculator',
  templateUrl: './calculator.component.html',
  styleUrls: ['./calculator.component.css']
})
export class CalculatorComponent implements OnInit {
  result:any = 0;
  constructor() { }

  ngOnInit(): void {
  }
  handTinh(number1:any ,number2:any ,method:any)
  {
    number1 =parseInt(number1)
    number2 =parseInt(number2)
    console.log(number1,method ,number2);
    switch(method){
        case 'cong':
         this.result = number1 + number2;
        break;
        case 'tru':
          this.result = number1-number2;
        break;
        case 'nhan':
          this.result = number1*number2;
        break;
        case 'chia':
          this.result = number1/number2;
        break;
    }
  }

}
